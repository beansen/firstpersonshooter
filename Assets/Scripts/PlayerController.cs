﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

	public AudioSource audioSource;

	public WeaponBehavior weaponBehavior;

	public ParticleSystem particleSystem;

	private float speed = 6f;
	private float sensivity = 5f;

	private float recoil = 10f;

	private float timer;

	private Transform camera;

	private bool escape;

	private bool aimingDownSight;

	// Use this for initialization
	void Start()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		camera = transform.GetChild(0);
	}

	// Update is called once per frame
	void Update()
	{
		if (!escape)
		{
			Movement();
			Rotation();
			MouseInput();
		}

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			escape = !escape;
			Cursor.lockState = escape ? CursorLockMode.None : CursorLockMode.Locked;
			Cursor.visible = escape;
		}
	}

	private void Movement()
	{
		Vector3 move = transform.forward;
		move.y = 0;
		transform.position += move * Input.GetAxis("Vertical") * Time.deltaTime * speed;

		move = transform.right;
		move.y = 0;
		transform.position += move * Input.GetAxis("Horizontal") * Time.deltaTime * speed;
	}

	private void Rotation()
	{
		transform.localRotation *= Quaternion.Euler(0f, Input.GetAxis("Mouse X") * sensivity, 0f);
		Quaternion rotation = camera.localRotation * Quaternion.Euler(-Input.GetAxis("Mouse Y") * sensivity, 0f, 0f);
		rotation.x /= rotation.w;
		rotation.w = 1f;

		float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(rotation.x);

		angleX = Mathf.Clamp(angleX, -90, 90);

		rotation.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

		camera.localRotation = rotation;
	}

	private void MouseInput()
	{
		if (Input.GetMouseButton(0))
		{
			if (!weaponBehavior.IsShooting)
			{
				weaponBehavior.Shoot();
				if (!audioSource.isPlaying)
				{
					audioSource.Play();
				}

				RaycastHit hit;
				Vector3 origin = camera.position;
				Vector3 direction = camera.forward;

				if (Physics.Raycast(origin, direction, out hit))
				{
					if (hit.transform.tag.Equals("Torso"))
					{
						hit.transform.parent.GetComponent<ZombieBehavior>().TakeDamage(40);
						particleSystem.transform.position = hit.point;
						particleSystem.Play();
					}

					if (hit.transform.tag.Equals("Head"))
					{
						hit.transform.parent.GetComponent<ZombieBehavior>().TakeDamage(60);
						particleSystem.transform.position = hit.point;
						particleSystem.Play();
					}
				}
			}
		}

		if (Input.GetMouseButtonDown(1)) {
			StartCoroutine(aimingDownSight ? AimHip() : AimDownSight());
			aimingDownSight = !aimingDownSight;
		}
	}

	private IEnumerator AimDownSight() {
		Camera cam = Camera.main;

		while (cam.fieldOfView > 45) {
			cam.fieldOfView = Mathf.MoveTowards(cam.fieldOfView, 45, Time.deltaTime * 20);
			yield return null;
		}
	}

	private IEnumerator AimHip() {
		Camera cam = Camera.main;

		while (cam.fieldOfView < 60)
		{
			cam.fieldOfView = Mathf.MoveTowards(cam.fieldOfView, 60, Time.deltaTime * 20);
			yield return null;
		}
	}
}
