﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.AI;

public class ZombieBehavior : MonoBehaviour
{

	private float speed = 1.5f;

	private int health = 100;

	private DateTime nextSpawnTime;

	private Animator animator;

	private NavMeshAgent agent;

	private bool canAttack = true;

	private float timer;

	public int Health {
		get { return health; }
	}

	public DateTime NextSpawnTime
	{
		get { return nextSpawnTime; }
	}

	public void Init() {
		animator = GetComponent<Animator>();
		agent = GetComponent<NavMeshAgent>();
		agent.speed = UnityEngine.Random.Range(3.5f, 5f);
	}

	public void SetDestination(Vector3 position) {
		agent.SetDestination(position);
		canAttack = true;
		timer = 0;
	}

	public bool Attack() {
		if (canAttack) {
			agent.ResetPath();
			canAttack = false;
			return true;
		} else {
			timer += Time.deltaTime;

			if (timer >= 1) {
				canAttack = true;
				timer = 0;
			}
			return false;
		}
	}

	public void TakeDamage(int damage)
	{
		health -= damage;

		if (health <= 0)
		{
			nextSpawnTime = DateTime.UtcNow.AddSeconds(UnityEngine.Random.Range(10, 16));
			agent.ResetPath();
			StartCoroutine(Die());
		}
	}

	public void Respawn(Vector3 position) {
		health = 100;
		transform.position = position;
		gameObject.SetActive(true);
		animator.SetBool("Dead", false);
	}

	private IEnumerator Die() {
		animator.SetBool("Dead", true);
		Vector3 rot = transform.localEulerAngles;
		rot.x = 0;
		transform.localEulerAngles = rot;
		yield return new WaitForSeconds(3);
		gameObject.SetActive(false);
	}
}
