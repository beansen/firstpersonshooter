﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ZombieController : MonoBehaviour {

	public Transform player;

	public GameObject zombiePrefab;

	private Dictionary<ZombieBehavior, int> zombies;

	private int index;

	// Use this for initialization
	void Start () {
		zombies = new Dictionary<ZombieBehavior, int>();
		StartCoroutine(SpawnZombies());
	}
	
	// Update is called once per frame
	void Update () {
		foreach (ZombieBehavior zombie in zombies.Keys)
		{
			if (zombie.gameObject.activeInHierarchy) {
				if (zombie.Health > 0) {
					if ((zombie.transform.position - player.position).sqrMagnitude > 5)
					{
						zombie.SetDestination(player.position);
					} else {
						bool attacked = zombie.Attack();
						if (attacked) {
							Debug.Log("Damage taken " + index);
							index++;
						}
					}
				} 
			} else {
				if (DateTime.UtcNow >= zombie.NextSpawnTime) {
					zombie.Respawn(GetSpawnPosition());
				}
			}
		}
	}

	private IEnumerator SpawnZombies() {
		for (int i = 0; i < 20; i++)
		{
			GameObject zombie = Instantiate(zombiePrefab);
			zombie.transform.position = GetSpawnPosition();
			ZombieBehavior zombieBehavior = zombie.AddComponent<ZombieBehavior>();
			zombieBehavior.Init();
			zombies.Add(zombieBehavior, 0);
			yield return new WaitForSeconds(3);
		}
	}

	private Vector3 GetSpawnPosition() {
		Vector3 pos = transform.GetChild(UnityEngine.Random.Range(0, transform.childCount)).position;
		return pos;
	}
}
