﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBehavior : MonoBehaviour {

	private Animator animator;

	private bool shooting;

	public bool IsShooting {
		get { return shooting; }
	}

	private void Start()
	{
		animator = GetComponent<Animator>();
	}

	public void Shoot() {
		StartCoroutine(ShootWeapon());
	}

	private IEnumerator ShootWeapon() {
		shooting = true;
		animator.SetBool("Shooting", true);
		yield return new WaitForSeconds(0.9f);
		animator.SetBool("Shooting", false);
		shooting = false;
	}
}
