## WIP (Not a version yet)

This will be a single player/coop survival game and 4v4/5v5 shooter.

##Coming features:

Survival:

- Survive different waves
- Better bot ai
- Actual map
- Pick up ammo, health
- Different weapons
- More sounds
- Different zombies
- Coop


Multiplayer:

- 4v4/5v5 tdm (maybe s&d too)
